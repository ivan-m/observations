{- |
   Module      : Main
   Description : Weather observation analysis
   Copyright   : Ivan Lazar Miljenovic
   License     : AllRightsReserved
   Maintainer  : Ivan.Miljenovic@gmail.com



 -}
module Main (main) where

import Observations.Analyse
import Observations.Generate
import Observations.Normalise
import Observations.Options

--------------------------------------------------------------------------------

main :: IO ()
main = execParser parseArgs >>= runCommand

runCommand :: Args -> IO ()
runCommand (Generate  ga) = runGenerate  ga
runCommand (Analyse   aa) = runAnalysis  aa
runCommand (Normalise na) = runNormalise na

--------------------------------------------------------------------------------
