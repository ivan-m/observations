{-# LANGUAGE RankNTypes #-}
{- |
   Module      : Observations.Analyse
   Description : Run analyses
   Copyright   : Ivan Lazar Miljenovic
   License     : AllRightsReserved
   Maintainer  : Ivan.Miljenovic@gmail.com



 -}
module Observations.Analyse where

import Observations.Aggregation
import Observations.Files
import Observations.Observatories
import Observations.Records
import Observations.Units.Distance
import Observations.Units.Temperature

import Control.Quiver.Sort (defaultConfig, spfilesortBy)
import Control.Quiver.SP

import Control.Monad                (when)
import Control.Monad.IO.Class       (liftIO)
import Control.Monad.Trans.Resource (ResourceT, runResourceT)
import Data.Function                (on)
import Data.Proxy

--------------------------------------------------------------------------------

data AnalyseArgs = AArgs { aTempUnit    :: !KnownTemp
                         , aDistUnit    :: !KnownDist
                         , getMinTemp   :: !Bool
                         , getMaxTemp   :: !Bool
                         , getMeanTemp  :: !Bool
                         , getObsCount  :: !Bool
                         , getDistance  :: !Bool
                         , aInputFile   :: !FilePath
                         , isNormalised :: !Bool
                         }
  deriving (Eq, Show, Read)

--------------------------------------------------------------------------------

runAnalysis :: AnalyseArgs -> IO ()
runAnalysis aa = withUnits $ \dp tp -> do
  mr <- runResourceT (sprun (pipeline dp tp))
  case mr of
    Nothing -> putStrLn "No results to show; was there any data?"
    Just r  -> printResults aa (calcResults r)

  where
    -- TODO: deal with possible exceptions

    pipeline :: (Distance d, Temperature t) => Proxy d -> Proxy t
                -> Effect (ResourceT IO) (Maybe (Aggregates d t))
    pipeline dp tp = qhoist liftIO (readRecordFile (aInputFile aa))
                     >>-> sppure (pickUnits dp tp)
                     >>-> spfilesortBy (compare `on` timestamp) defaultConfig
                     >>-> calculateAggregates
                     >&> snd

    pickUnits dp dt
      | isNormalised aa = setUnits dp dt
      | otherwise       = setFromDefault dp dt

    withUnits :: (forall d t. (Distance d, Temperature t) => Proxy d -> Proxy t -> k) -> k
    withUnits k = withKnownDist (aDistUnit aa) $ \dp ->
                    withKnownTemp (aTempUnit aa) $ \tp ->
                      k dp tp

printResults :: (Distance d, Temperature t) => AnalyseArgs -> Results d t -> IO ()
printResults aa res = do
  putStrLn "Requested results: "
  printTemp getMinTemp  minTemp  "Minimum"
  printTemp getMaxTemp  maxTemp  "Maximum"
  printTemp getMeanTemp meanTemp "Mean"
  when (getDistance aa) (printInd $ "Total distance: " ++ prettyDist (totalDist res))
  when (getObsCount aa) (printInd "Count by observatory:" >> mapM_ printCnt (obsCount res))
  where
    printTemp fa fr lb = when (fa aa) (printInd $ lb ++ " temperature: " ++ prettyTemp (fr res))

    printInd = putStrLn . ('\t':)

    printCnt (obs,cnt) = printInd $ '\t' : prettyObs obs ++ ": " ++ show cnt
