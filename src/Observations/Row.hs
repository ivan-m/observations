{-# LANGUAGE PatternSynonyms, RankNTypes #-}

{- |
   Module      : Observations.Row
   Description : Input/output of rows
   Copyright   : Ivan Lazar Miljenovic
   License     : AllRightsReserved
   Maintainer  : Ivan.Miljenovic@gmail.com

   Based very loosely upon quiver-csv

 -}
module Observations.Row where

import Control.Quiver.SP
import Data.Cell

import           Data.ByteString (ByteString)
import qualified Data.ByteString as B

import Data.Word (Word8)

--------------------------------------------------------------------------------


-- | @\n@
pattern LF :: Word8
pattern LF = 0x0A

-- | @\r@
pattern CR :: Word8
pattern CR = 0x0D

-- | @|@
pattern TAB :: Word8
pattern TAB = 0x7C

fieldSeparator :: ByteString
fieldSeparator = B.singleton TAB

rowSeparator :: ByteString
rowSeparator = B.pack [CR, LF]

infixr 5 *>:>

-- | Emits a bytestring, filtering out the empty ones.

(*>:>) :: ByteString -> P a' a ByteString b' f (SPResult e) -> P a' a ByteString b' f (SPResult e)
y *>:> sp
  | B.null y  = sp
  | otherwise = produce y (const sp) spincomplete



--------------------------------------------------------------------------------

-- | Very simplistically split inputs along @'|'@ and any newline
--   (@'\n'@ and @'\r'@).
decodeTSV :: SP ByteString (Cell ByteString) f ()
decodeTSV = decodeC
  where
    -- Decode a cell
    decodeC = spconsume decodeC' spcomplete

    decodeC' s =
      case B.uncons s of
        Just (c, xs) ->
          case c of
            TAB -> Cell B.empty EOC >:> decodeC'  xs
            CR  -> Cell B.empty EOR >:> decodeCR' xs
            LF  -> Cell B.empty EOR >:> decodeC'  xs
            _   -> decodeF' s 1 xs
        Nothing -> decodeC

    -- Found a CR
    decodeCR = spconsume decodeCR' spincomplete
    decodeCR' s =
      case B.uncons s of
        Just (c, xs) ->
          case c of
            TAB -> Cell B.empty EOC >:> decodeC'  xs
            CR  -> Cell B.empty EOR >:> decodeCR' xs
            LF  -> decodeC' xs -- Part of same newline break as before
            _   -> decodeF' s 1 xs
        Nothing -> decodeCR

    -- Decode a field
    decodeF' s i s' =
      case B.uncons s' of
        Just (c, xs) ->
          case c of
            TAB -> Cell (B.take i s) EOC >:> decodeC'  xs
            CR  -> Cell (B.take i s) EOR >:> decodeCR' xs
            LF  -> Cell (B.take i s) EOR >:> decodeC'  xs
            _   -> let i' = i + 1 in i' `seq` decodeF' s i' xs
        Nothing -> decodeV s

    -- Decode an unquoted field after a non-empty part @ps@ has been identified:
    decodeV ps = spconsume (decodeV' ps) (Cell ps EOR >:> spfailed ())
    decodeV' ps s =
      case B.uncons s of
        Just (c, xs) ->
          case c of
            TAB -> Cell ps EOC >:> decodeC'  xs
            CR  -> Cell ps EOR >:> decodeCR' xs
            LF  -> Cell ps EOR >:> decodeC'  xs
            _   -> Cell ps EOP >:> decodeF' s 1 xs
        Nothing -> decodeV ps

-- | Assume fields don't contain any characters that need to be escaped.
encodeTSV :: SP (Cell ByteString) ByteString f ()
encodeTSV = loop
  where
    loop = spconsume loop' spcomplete

    loop' (Cell x EOP) = x *>:> loop1
    loop' (Cell x EOC) = x *>:> fieldSeparator >:> loop2
    loop' (Cell x EOR) = x *>:> rowSeparator >:> loop
    loop' (Cell x EOT) = x *>:> rowSeparator >:> loop

    -- mid-way through a cell
    loop1 = spconsume loop1' spincomplete
    loop1' (Cell x EOP) = x *>:> loop1
    loop1' (Cell x EOC) = x *>:> fieldSeparator >:> loop2
    loop1' (Cell x EOR) = x *>:> rowSeparator >:> loop
    loop1' (Cell x EOT) = x *>:> rowSeparator >:> loop

    -- mid-way through a row
    loop2 = spconsume loop' (spfailed ())
