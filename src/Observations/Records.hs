{-# LANGUAGE DeriveAnyClass, DeriveGeneric, GADTs, OverloadedStrings,
             PatternGuards, RankNTypes, TypeApplications #-}

{- |
   Module      : Observations.Records
   Description : Recorded values
   Copyright   : Ivan Lazar Miljenovic
   License     : AllRightsReserved
   Maintainer  : Ivan.Miljenovic@gmail.com



 -}
module Observations.Records where

import Observations.Observatories
import Observations.Units.Distance
import Observations.Units.Temperature

import           Data.Binary           (Binary)
import           Data.Binary.Orphans   ()
import           Data.ByteString       (ByteString)
import qualified Data.ByteString.Char8 as B
import           Data.Monoid           ((<>))
import           Data.Proxy
import           Data.Time.Clock       (UTCTime)
import           Data.Time.Format      (TimeLocale(..), defaultTimeLocale,
                                        formatTime, iso8601DateFormat,
                                        parseTimeM)
import           Data.Time.LocalTime   (timeZoneName)
import           GHC.Generics          (Generic)

--------------------------------------------------------------------------------

data Record d t = Record { timestamp   :: !UTCTime
                         , location    :: !(Coord d)
                         , temperature :: !t
                         , observatory :: !Observatory
                         }
  deriving (Eq, Ord, Show, Read, Generic, Binary)

type ParsedRecord = Record Integer Integer

--------------------------------------------------------------------------------

parseRow :: [ByteString] -> Maybe ParsedRecord
parseRow [ts,loc,temp,obs] = Record <$> parseTimestamp ts
                                    <*> parseCoord loc
                                    <*> parseInteger temp
                                    <*> parseObservatory obs
parseRow _                 = Nothing

parseTimestamp :: ByteString -> Maybe UTCTime
parseTimestamp = parseTimeM False utcTimeLocale timeFormatString
                 . B.unpack

utcTimeLocale :: TimeLocale
utcTimeLocale = defaultTimeLocale { knownTimeZones = utc }
  where
    utc = filter ((`elem` ["UT", "GMT"]) . timeZoneName) (knownTimeZones defaultTimeLocale)

timeFormatString :: String
timeFormatString = iso8601DateFormat (Just "%H:%M")

parseObservatory :: ByteString -> Maybe Observatory
parseObservatory bs = case bs of
                        "AU" -> Just AU
                        "US" -> Just US
                        "FR" -> Just FR
                        _ | B.length bs == 2 && B.all isUpper bs -> Just (Other bs)
                        _    -> Nothing
  where
    -- Just in case random raw bytes somehow trips up Data.Char.isUpper
    isUpper c = 'A' <= c && c <= 'Z'

parseNatural :: ByteString -> Maybe Integer
parseNatural bs
  | B.null bs        = Nothing
  | B.all isDigit bs = Just . read . B.unpack $ bs
  | otherwise        = Nothing
  where
    -- Just in case random raw bytes somehow trips up Data.Char.isDigit
    isDigit c = '0' <= c && c <= '9'

parseInteger :: ByteString -> Maybe Integer
parseInteger bs = case B.uncons bs of
                    Just ('-', bs') -> negate <$> parseNatural bs'
                    _               -> parseNatural bs

parseCoord :: ByteString -> Maybe (Coord Integer)
parseCoord bs = case B.break (==',') bs of
                  (bx,cy) | Just (',', by) <- B.uncons cy -> Coord <$> parseNatural bx
                                                                   <*> parseNatural by
                  _                                       -> Nothing

--------------------------------------------------------------------------------

printRecord :: (Distance d, Temperature t) => Record d t -> [ByteString]
printRecord r = [ printTimestamp (timestamp r)
                , printCoord (location r)
                , printAsInt (fromTemperature (temperature r))
                , printObs (observatory r)
                ]
  where
    printTimestamp = B.pack . formatTime utcTimeLocale timeFormatString

    printCoord c = printAsInt (fromDistance (x c)) <> "," <> printAsInt (fromDistance (y c))

    printAsInt = B.pack . show . round @Double @Int

    printObs AU          = "AU"
    printObs US          = "US"
    printObs FR          = "FR"
    printObs (Other obs) = obs

--------------------------------------------------------------------------------

setUnits :: (Distance d, Temperature t) => Proxy d -> Proxy t -> ParsedRecord -> Record d t
setUnits _ _ r = Record { timestamp   = timestamp r
                        , location    = makeDistance . fromInteger <$> location r
                        , temperature = makeTemperature . fromInteger $ temperature r
                        , observatory = observatory r
                        }

setFromDefault :: (Distance d, Temperature t) => Proxy d -> Proxy t -> ParsedRecord -> Record d t
setFromDefault _ _ r = withDefaults obs $ \dp tp ->
  Record { timestamp   = timestamp r
         , location    = convertDistance . makeDistanceFrom dp . fromInteger <$> location r
         , temperature = convertTemperature . makeTemperatureFrom tp . fromInteger $ temperature r
         , observatory = obs
         }
  where
    obs = observatory r
