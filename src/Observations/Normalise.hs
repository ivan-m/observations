{-# LANGUAGE RankNTypes #-}
{- |
   Module      : Observations.Normalise
   Description : Normalise input
   Copyright   : Ivan Lazar Miljenovic
   License     : AllRightsReserved
   Maintainer  : Ivan.Miljenovic@gmail.com



 -}
module Observations.Normalise where

import Observations.Files
import Observations.Records
import Observations.Units.Distance
import Observations.Units.Temperature

import Control.Quiver.SP
import Data.Proxy

--------------------------------------------------------------------------------

data NormaliseArgs = NArgs { nTempUnit   :: !KnownTemp
                           , nDistUnit   :: !KnownDist
                           , nInputFile  :: !FilePath
                           , nOutputFile :: !FilePath
                           }
  deriving (Eq, Show, Read)

--------------------------------------------------------------------------------

runNormalise :: NormaliseArgs -> IO ()
runNormalise na = withUnits $ \dp tp -> sprun (pipeline dp tp)
  where
    -- TODO: deal with possible exceptions

    pipeline dp dt = readRecordFile (nInputFile na)
                     >>-> sppure (setFromDefault dp dt)
                     >>-> writeRecordFile (nOutputFile na)
                     >&> (const ())

    withUnits :: (forall d t. (Distance d, Temperature t) => Proxy d -> Proxy t -> k) -> k
    withUnits k = withKnownDist (nDistUnit na) $ \dp ->
                    withKnownTemp (nTempUnit na) $ \tp ->
                      k dp tp
