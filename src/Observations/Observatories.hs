{-# LANGUAGE DeriveAnyClass, DeriveGeneric, OverloadedStrings, RankNTypes,
             TypeApplications #-}

{- |
   Module      : Observations.Observatories
   Description : Known observatories
   Copyright   : Ivan Lazar Miljenovic
   License     : AllRightsReserved
   Maintainer  : Ivan.Miljenovic@gmail.com



 -}
module Observations.Observatories where

import Observations.Units.Distance
import Observations.Units.Temperature

import           Data.Binary               (Binary)
import           Data.ByteString.Char8     (ByteString, unpack)
import qualified Data.ByteString.Char8     as B
import           Data.Proxy
import           GHC.Generics              (Generic)
import           Test.QuickCheck           (Arbitrary(..), Gen, choose,
                                            frequency, listOf, suchThat)
import           Test.QuickCheck.Instances ()

--------------------------------------------------------------------------------

data Observatory = AU | US | FR | Other ByteString
  deriving (Eq, Ord, Show, Read, Generic, Binary)

instance Arbitrary Observatory where
  arbitrary = frequency [ (10, return AU)
                        , (10, return US)
                        , (10, return FR)
                        -- More likely to get a 2-digit one
                        , (3,  Other . B.take 2 <$> genOther)
                        , (1,  Other <$> genOther)
                        ]

-- Only capital letters
genOther :: Gen ByteString
genOther = (B.pack <$> listOf (choose ('A', 'Z')))
           `suchThat`
           ((`notElem` ["AU", "US", "FR"]) . B.take 2)

withDefaults :: Observatory
                -> (forall d t. (Distance d, Temperature t) => Proxy d -> Proxy t -> k)
                -> k
withDefaults obs k = defaultDistance obs $ \dp ->
                       defaultTemperature obs $ \tp ->
                         k dp tp

defaultDistance :: Observatory -> (forall d. (Distance d) => Proxy d -> k) -> k
defaultDistance AU      k = k (Proxy @Kilometres)
defaultDistance US      k = k (Proxy @Miles)
defaultDistance FR      k = k (Proxy @Metres)
defaultDistance Other{} k = k (Proxy @Kilometres)

defaultTemperature :: Observatory -> (forall d. (Temperature d) => Proxy d -> k) -> k
defaultTemperature AU      k = k (Proxy @Celsius)
defaultTemperature US      k = k (Proxy @Fahrenheit)
defaultTemperature FR      k = k (Proxy @Kelvin)
defaultTemperature Other{} k = k (Proxy @Kelvin)

prettyObs :: Observatory -> String
prettyObs AU        = "AU"
prettyObs US        = "US"
prettyObs FR        = "FR"
prettyObs (Other o) = unpack o
