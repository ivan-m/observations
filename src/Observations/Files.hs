{-# LANGUAGE RankNTypes #-}
{- |
   Module      : Observations.Files
   Description : Read/Write observation files
   Copyright   : Ivan Lazar Miljenovic
   License     : AllRightsReserved
   Maintainer  : Ivan.Miljenovic@gmail.com



 -}
module Observations.Files where

import Observations.Records
import Observations.Row
import Observations.Units.Distance    (Distance)
import Observations.Units.Temperature (Temperature)

import Control.Quiver.ByteString
import Control.Quiver.Cell
import Control.Quiver.SP

import Control.Exception (IOException)
import Data.ByteString   (ByteString)

--------------------------------------------------------------------------------

readRecordFile :: FilePath -> SProducer ParsedRecord IO IOException
readRecordFile fl = qReadFile fl 4096
                    >>-> decodeTSV           >&> fst
                    >>-> toRows              >&> fst
                    >>-> spMapMaybe parseRow >&> fst

writeRecordFile :: (Distance d, Temperature t)
                   => FilePath -> SConsumer (Record d t) IO IOException
writeRecordFile fl = sppure printRecord
                     >>-> writeRows fl >&> snd

writeRows :: FilePath -> SConsumer [ByteString] IO IOException
writeRows fl = fromRows
               >>-> encodeTSV
               >>-> qWriteFile fl >&> snd

--------------------------------------------------------------------------------

spMapMaybe :: (a -> Maybe b) -> SP a b f ()
spMapMaybe f = loop
  where
    loop = spconsume (go . f) spcomplete

    go mb = maybe id (>:>) mb loop
