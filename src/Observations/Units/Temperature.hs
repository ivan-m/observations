{-# LANGUAGE DefaultSignatures, FlexibleContexts, GeneralizedNewtypeDeriving,
             MultiParamTypeClasses, RankNTypes, TypeApplications, TypeFamilies
             #-}

{- |
   Module      : Observations.Temperature
   Description : Temperature definitions
   Copyright   : Ivan Lazar Miljenovic
   License     : AllRightsReserved
   Maintainer  : Ivan.Miljenovic@gmail.com



 -}
module Observations.Units.Temperature where

import Data.Binary     (Binary)
import Data.Coerce     (Coercible, coerce)
import Data.Proxy
import System.Random   (Random(..))
import Test.QuickCheck (Arbitrary(..), choose)

--------------------------------------------------------------------------------

newtype Celsius = C Double
  deriving (Eq, Ord, Show, Read, Num, Real, Fractional, Floating, Binary, Random)

instance Arbitrary Celsius where
  arbitrary = choose (minTemperature, maxTemperature)

newtype Fahrenheit = F Double
  deriving (Eq, Ord, Show, Read, Num, Real, Fractional, Floating, Binary, Random)

instance Arbitrary Fahrenheit where
  arbitrary = choose (minTemperature, maxTemperature)

newtype Kelvin = K Double
  deriving (Eq, Ord, Show, Read, Num, Real, Fractional, Floating, Binary, Random)

instance Arbitrary Kelvin where
  arbitrary = choose (minTemperature, maxTemperature)

-- Conversions taken from https://en.wikipedia.org/wiki/Conversion_of_units_of_temperature

class (Real t, Floating t, Binary t, Arbitrary t, Show t) => Temperature t where
  fromKelvin :: Kelvin -> t

  toKelvin :: t -> Kelvin

  makeTemperature :: Double -> t
  default makeTemperature :: (Coercible Double t) => Double -> t
  makeTemperature = coerce

  fromTemperature :: t -> Double
  default fromTemperature :: (Coercible t Double) => t -> Double
  fromTemperature = coerce

  prettyTemp :: t -> String

instance Temperature Celsius where
  fromKelvin (K k) = C (k - 273.15)

  toKelvin (C c) = K (c + 273.15)

  prettyTemp (C c) = show c ++ " ℃"

instance Temperature Fahrenheit where
  fromKelvin (K k) = F (k * 9/5 - 459.67)

  toKelvin (F f) = K ((f + 459.67) * 5 / 9)

  prettyTemp (F f) = show f ++ " ℉"

instance Temperature Kelvin where
  fromKelvin = id

  toKelvin = id

  prettyTemp (K k) = show k ++ " K"

makeTemperatureFrom :: (Temperature t) => proxy t -> Double -> t
makeTemperatureFrom _ = makeTemperature

convertTemperature :: (Temperature f, Temperature t) => f -> t
convertTemperature = fromKelvin . toKelvin
{-# INLINE convertTemperature #-}

-- TODO: RULES

--------------------------------------------------------------------------------

-- | Taken from <https://en.wikipedia.org/wiki/Lowest_temperature_recorded_on_Earth>
minTemperature :: (Temperature t) => t
minTemperature = fromKelvin 178
{-# INLINE minTemperature  #-}

-- | Taken from <https://en.wikipedia.org/wiki/Highest_temperature_recorded_on_Earth>
maxTemperature :: (Temperature t) => t
maxTemperature = fromKelvin 373.15 -- 100 C, theoretical max ground temperature
{-# INLINE maxTemperature #-}

--------------------------------------------------------------------------------

data KnownTemp = Celsius | Fahrenheit | Kelvin
  deriving (Eq, Ord, Show, Read)

withKnownTemp :: KnownTemp -> (forall t. (Temperature t) => Proxy t -> k) -> k
withKnownTemp Celsius k    = k (Proxy @Celsius)
withKnownTemp Fahrenheit k = k (Proxy @Fahrenheit)
withKnownTemp Kelvin k     = k (Proxy @Kelvin)
