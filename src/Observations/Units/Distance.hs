{-# LANGUAGE DefaultSignatures, DeriveFunctor, DeriveGeneric, FlexibleContexts,
             GeneralizedNewtypeDeriving, MultiParamTypeClasses, RankNTypes,
             TypeApplications, TypeFamilies #-}

{- |
   Module      : Observations.Distance
   Description : Distance definitions
   Copyright   : Ivan Lazar Miljenovic
   License     : AllRightsReserved
   Maintainer  : Ivan.Miljenovic@gmail.com



 -}
module Observations.Units.Distance where

import Data.Binary     (Binary)
import Data.Coerce     (Coercible, coerce)
import Data.Fixed      (mod')
import Data.Proxy
import GHC.Generics    (Generic)
import System.Random   (Random(..))
import Test.QuickCheck (Arbitrary(..), choose)

--------------------------------------------------------------------------------

newtype Kilometres = KM Double
  deriving (Eq, Ord, Show, Read, Num, Real, Fractional, Floating, Binary, Random)

newtype Metres = M Double
  deriving (Eq, Ord, Show, Read, Num, Real, Fractional, Floating, Binary, Random)

newtype Miles = Mi Double
  deriving (Eq, Ord, Show, Read, Num, Real, Fractional, Floating, Binary, Random)

class (Real d, Floating d, Binary d, Random d, Show d) => Distance d where
  fromMetres :: Metres -> d

  toMetres :: d -> Metres

  makeDistance :: Double -> d
  default makeDistance :: (Coercible Double d) => Double -> d
  makeDistance = coerce

  fromDistance :: d -> Double
  default fromDistance :: (Coercible d Double) => d -> Double
  fromDistance = coerce

  prettyDist :: d -> String

instance Distance Kilometres where
  fromMetres (M m) = KM (m / 1000)

  toMetres (KM km) = M (km * 1000)

  prettyDist (KM km) = show km ++ " km"

instance Distance Metres where
  fromMetres = id

  toMetres = id

  prettyDist (M m) = show m ++ " m"

instance Distance Miles where
  fromMetres (M m) = Mi (m / 1609.344)

  toMetres (Mi mi) = M (mi * 1609.344)

  prettyDist (Mi mi) = show mi ++ " Mi"

makeDistanceFrom :: (Distance d) => proxy d -> Double -> d
makeDistanceFrom _ = makeDistance

convertDistance :: (Distance f, Distance t) => f -> t
convertDistance = fromMetres . toMetres
{-# INLINE convertDistance #-}

-- TODO: RULES

--------------------------------------------------------------------------------

data Coord d = Coord { x :: !d
                     , y :: !d
                     }
  deriving (Eq, Ord, Show, Read, Functor, Generic)

instance (Binary d) => Binary (Coord d)

instance (Distance d) => Arbitrary (Coord d) where
  arbitrary = Coord <$> choose (0, circumEarth)
                    <*> choose (0, halfCircumEarth)

-- | Based upon the <https://en.wikipedia.org/wiki/World_Geodetic_System#WGS84 WGS84 specification>
radiusEarth :: (Distance d) => d
radiusEarth = fromMetres 6378137
{-# INLINE radiusEarth #-}

halfCircumEarth :: (Distance d) => d
halfCircumEarth = pi * radiusEarth
{-# INLINE halfCircumEarth #-}

circumEarth :: (Distance d) => d
circumEarth = 2 * halfCircumEarth
{-# INLINE circumEarth #-}

-- | Calculated using Euclidean distance using a
--  rectangular/cylindrical projection.
distanceBetween :: (Distance d) => Coord d -> Coord d -> d
distanceBetween c1 c2 = sqrt (dx^2 + dy^2)
  where
    dx = abs (x c2 - x c1) `mod'` circumEarth
    dy = abs (y c2 - y c1) `mod'` halfCircumEarth

--------------------------------------------------------------------------------

data KnownDist = Metres | Kilometres | Miles
  deriving (Eq, Ord, Show, Read)

withKnownDist :: KnownDist -> (forall d. (Distance d) => Proxy d -> k) -> k
withKnownDist Metres k     = k (Proxy @Metres)
withKnownDist Kilometres k = k (Proxy @Kilometres)
withKnownDist Miles k      = k (Proxy @Miles)
