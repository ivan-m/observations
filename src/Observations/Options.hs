{- |
   Module      : Observations.Options
   Description : Parse command-line options
   Copyright   : Ivan Lazar Miljenovic
   License     : AllRightsReserved
   Maintainer  : Ivan.Miljenovic@gmail.com



 -}
module Observations.Options
  ( Args (..)
  , parseArgs
  -- Re-export
  , execParser
  ) where

import Observations.Analyse
import Observations.Generate
import Observations.Normalise
import Observations.Units.Distance
import Observations.Units.Temperature

import Data.Monoid         ((<>))
import Options.Applicative

--------------------------------------------------------------------------------

data Args = Generate  GenerateArgs
          | Analyse   AnalyseArgs
          | Normalise NormaliseArgs
  deriving (Eq, Show, Read)

parseArgs :: ParserInfo Args
parseArgs = info (helper <*> prs) fullDesc
  where
    prs = subparser (   command "generate"  (Generate  <$> parseGenerate)
                     <> command "analyse"   (Analyse   <$> parseAnalyse)
                     <> command "normalise" (Normalise <$> parseNormalise)
                    )

parseGenerate :: ParserInfo GenerateArgs
parseGenerate = info (helper <*> prs)
                     (progDesc "Generate sample values")
  where
    prs = GArgs <$> option auto (   long "num"
                                 <> short 'n'
                                 <> metavar "NUM_LINES"
                                 <> value 1000
                                 <> showDefault
                                 <> help "Number of lines to generate"
                                )
                <*> outputFile

parseAnalyse :: ParserInfo AnalyseArgs
parseAnalyse = info (helper <*> prs)
                    (progDesc "Analyse provided values")
  where
    prs = AArgs <$> parseTemp
                <*> parseDist
                <*> calcSwitch "min"    "minimum temperature"
                <*> calcSwitch "max"    "maximum temperature"
                <*> calcSwitch "mean"   "mean temperature"
                <*> calcSwitch "counts" "observation counts per observatory"
                <*> calcSwitch "dist"   "total distance travelled"
                <*> inputFile
                <*> switch (   long "normalised"
                            <> showDefault
                            <> help "Observations are already normalised in specified units"
                           )

    calcSwitch sw desc = switch (  long sw
                                 <> showDefault
                                 <> help ("Calculate " ++ desc)
                                )

parseNormalise :: ParserInfo NormaliseArgs
parseNormalise = info (helper <*> prs)
                      (progDesc "Normalise provided values")
  where
    prs = NArgs <$> parseTemp
                <*> parseDist
                <*> inputFile
                <*> outputFile

parseTemp :: Parser KnownTemp
parseTemp = option (maybeReader getT)
                   (   long "temperature"
                    <> short 't'
                    <> metavar "UNIT"
                    -- TODO: do this properly
                    <> value Kelvin
                    <> showDefaultWith (const "K")
                    <> help "Temperature units to use (C, F or K)"
                   )
  where
    getT "C" = Just Celsius
    getT "F" = Just Fahrenheit
    getT "K" = Just Kelvin
    getT _   = Nothing

parseDist :: Parser KnownDist
parseDist = option (maybeReader getD)
                   (   long "distance"
                    <> short 'd'
                    <> metavar "UNIT"
                    -- TODO: do this properly
                    <> value Metres
                    <> showDefaultWith (const "M")
                    <> help "Distance units to use (M, KM or Mi)"
                   )
  where
    getD "M"  = Just Metres
    getD "KM" = Just Kilometres
    getD "Mi" = Just Miles
    getD _    = Nothing

inputFile :: Parser FilePath
inputFile = strOption (   long "input"
                       <> short 'i'
                       <> metavar "FILE"
                       <> help "Input file"
                      )

outputFile :: Parser FilePath
outputFile = strOption (   long "output"
                        <> short 'o'
                        <> metavar "FILE"
                        <> help "Output file"
                       )
