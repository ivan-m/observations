{- |
   Module      : Observations.Aggregation
   Description : Perform aggregation on results
   Copyright   : Ivan Lazar Miljenovic
   License     : AllRightsReserved
   Maintainer  : Ivan.Miljenovic@gmail.com



 -}
module Observations.Aggregation where

import Observations.Observatories
import Observations.Records
import Observations.Units.Distance
import Observations.Units.Temperature

import           Control.Monad.Trans.State.Strict
import           Control.Quiver.SP
import           Control.Quiver.Trans
import           Data.Map.Strict                  (Map)
import qualified Data.Map.Strict                  as M

--------------------------------------------------------------------------------

data Aggregates d t = Agg { minT    :: !t
                          , maxT    :: !t
                          , sumT    :: !t
                          , totalD  :: !d
                          , lastLoc :: {-# UNPACK #-} !(Coord d)
                          , obsCnt  :: !(Map Observatory Int)
                          }
  deriving (Eq, Ord, Show, Read)

initRecord :: (Distance d) => Record d t -> Aggregates d t
initRecord r = Agg { minT    = t
                   , maxT    = t
                   , sumT    = t
                   , totalD  = 0
                   , lastLoc = location r
                   , obsCnt  = M.singleton (observatory r) 1
                   }
  where
    t = temperature r

addRecord :: (Distance d, Temperature t) => Record d t -> Aggregates d t -> Aggregates d t
addRecord r agg = agg { minT    = min (minT agg) t
                      , maxT    = max (maxT agg) t
                      , sumT    = sumT agg + t
                      , totalD  = totalD agg + distanceBetween (lastLoc agg) l
                      , lastLoc = l
                      , obsCnt  = M.alter (Just . maybe 1 succ) (observatory r) (obsCnt agg)
                      }
  where
    t = temperature r

    l = location r

--------------------------------------------------------------------------------

calculateAggregates :: (Monad f, Distance d, Temperature t)
                       => Consumer () (Record d t) f (Maybe (Aggregates d t))
calculateAggregates = consume () startAgg (deliver Nothing)
  where
    startAgg r = qRunStrictStateT loop (initRecord r) >&> (Just . snd)

    loop = sptraverse_ (modify . addRecord)

--------------------------------------------------------------------------------

data Results d t = Results { minTemp   :: !t
                           , maxTemp   :: !t
                           , meanTemp  :: !t
                           , totalDist :: !d
                           , obsCount  :: ![(Observatory, Int)]
                           }
  deriving (Eq, Ord, Show, Read)

calcResults :: (Temperature t) => Aggregates d t -> Results d t
calcResults agg = Results { minTemp   = minT agg
                          , maxTemp   = maxT agg
                          , meanTemp  = sumT agg / fromIntegral cnt
                          , totalDist = totalD agg
                          , obsCount  = M.toList (obsCnt agg)
                          }
  where
    cnt = sum . M.elems $ obsCnt agg
