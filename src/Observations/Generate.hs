{-# LANGUAGE GADTs, RankNTypes, StandaloneDeriving #-}
{- |
   Module      : Observations.Generate
   Description : Generate values
   Copyright   : Ivan Lazar Miljenovic
   License     : AllRightsReserved
   Maintainer  : Ivan.Miljenovic@gmail.com



 -}
module Observations.Generate where

import Observations.Files
import Observations.Observatories
import Observations.Records
import Observations.Units.Distance
import Observations.Units.Temperature

import Test.QuickCheck
import Test.QuickCheck.Instances ()

import Control.Quiver.SP

import           Control.Monad         (replicateM, zipWithM)
import           Data.Bool             (bool)
import           Data.ByteString       (ByteString)
import qualified Data.ByteString       as B
import qualified Data.ByteString.Char8 as C
import           Data.Char
import           Data.Proxy

--------------------------------------------------------------------------------

data GenerateArgs = GArgs { numLines    :: !Int
                          , gOutputFile :: !FilePath
                          }
  deriving (Eq, Show, Read)

--------------------------------------------------------------------------------

runGenerate :: GenerateArgs -> IO ()
runGenerate ga = sprun pipeline
  where
    pipeline :: Effect IO ()
    pipeline = generateTestData (numLines ga)
               >->> sppure printTestData
               >->> writeRows (gOutputFile ga)
               >&> const ()

--------------------------------------------------------------------------------

data TestData = ValidRecord RawRecord
              | MissingCell CellNumber RawRecord -- Int is 0..3
              | ExtraCell Bool RawRecord ByteString
              | Corrupted [ByteString]
              | Garbage ByteString
  deriving (Show)

instance Arbitrary TestData where
  arbitrary = frequency [ (100, ValidRecord <$> arbitrary)
                        , (20,  MissingCell <$> genCellNumber <*> arbitrary)
                        , (20,  ExtraCell   <$> arbitrary <*> arbitrary <*> scale (`max`10) genValidBS)
                        , (5,   Corrupted   <$> genCorrupted)
                        , (1,   Garbage     <$> scale (`max` 30) genValidBS)
                        ]

generateTestData :: Int -> SProducer TestData IO ()
generateTestData c = spevery batchSizes
                     >>-> sptraverse genBatch
                     >>-> spconcat
                     >&> snd
  where
    genBatch l = generate (replicateM l (resize 50 arbitrary))

    batchSize = 1000

    (numBatch, extra) = c `quotRem` batchSize

    batchSizes = extra : replicate numBatch batchSize

printTestData :: TestData -> [ByteString]
printTestData (ValidRecord raw)    = printRaw raw
printTestData (MissingCell c raw)  = printMissing c raw
printTestData (ExtraCell f raw ex) = printExtra f raw ex
printTestData (Corrupted cptd)     = cptd
printTestData (Garbage gbg)        = [gbg]

data RawRecord where
  Raw :: forall d t. (Distance d, Temperature t) => Record d t -> RawRecord

deriving instance Show RawRecord

instance Arbitrary RawRecord where
  arbitrary = do
    obs <- arbitrary
    withDefaults obs $ \dp tp ->
      Raw <$> genRecord dp tp obs
   where
     genRecord :: (Distance d, Temperature t) => Proxy d -> Proxy t -> Observatory
                  -> Gen (Record d t)
     genRecord _ _ obs = Record <$> arbitrary
                                <*> arbitrary
                                <*> arbitrary
                                <*> pure obs

printRaw :: RawRecord -> [ByteString]
printRaw (Raw r) = printRecord r

printMissing :: CellNumber -> RawRecord -> [ByteString]
printMissing c raw =
  case printRaw raw of
    [ts,l,t,o] | c == 0 -> [l,t,o]
               | c == 1 -> [ts,t,o]
               | c == 2 -> [ts,l,o]
               | c == 3 -> [ts,l,t]
    wtf                 -> wtf -- Shouldn't happen

printExtra :: Bool -> RawRecord -> ByteString -> [ByteString]
printExtra front raw extra = bool (++[extra]) (extra:) front (printRaw raw)

-- | 0..3
type CellNumber = Int

genCellNumber :: Gen CellNumber
genCellNumber = choose (0,3)

genValidBS :: Gen ByteString
genValidBS = B.pack <$> listOf (choose (32, 122))

genCorrupted :: Gen [ByteString]
genCorrupted = do
  rawRec <- arbitrary
  let row = printRaw rawRec
  mapM corruptCol row
  where
    genDoCorrupt = frequency [(5, pure False), (1, pure True)]

    corruptCol :: ByteString -> Gen ByteString
    corruptCol b = do corB <- replicateM (B.length b) genDoCorrupt
                      C.pack <$> zipWithM (bool return corrupt) corB
                                                                (C.unpack b)

    corrupt :: Char -> Gen Char
    corrupt c
      | isAlpha c = choose ('0', '9')
      | isDigit c = choose (' ', '/') -- space and (safe) punctuation
      | otherwise = choose ('A', 'Z')
