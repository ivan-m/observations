Weather Observations
====================

The specified requirements for this project can be found
in [the requirements file](Requirements.md).

Assumptions made
----------------

* "Natural numbers" refer to the set $ℕ ≡ {∀ i ∈ ℤ, i ≥ 0}$

* The $x$ and $y$ co-ordinates are in units of the specified distance
  measure from some specified meridian and circle of latitude
  respectively (i.e. longitude and latitude).

* Balloon travel distance is calculated with respect to the ground
  distance using an approximation of the Earth (as no height is
  specified for the balloon).

* For the purposes of distance calculation, since the meridian and
  circle of latitude have not been specified and thus longitude and
  latitude cannot be calculated, a rectangular/cylindrical projection
  of the Earth is used (with equidistant measurements) with $0 ≤ x <
  c$ and $0 ≤ y < ᶜ/₂$ (where $c$ is the average circumference of the
  Earth).

* The international definition of mile is used.

* Observatory codes always consist of two capital English letters.

* When generating test data, actual Earth specifications for distance
  and temperatures (loosely based upon typical minimum and maximum
  temperatures) in the required units will be used to bound the
  values.

* Outputted values are rounded to the nearest integer.

Limitations
-----------

* Parsing and printing is overly simplistic, often going via Strings.

* In particular, parsing of the raw data could take into account
  fixed-width size of expected values (e.g. timestamps)

* I/O exceptions are ignored everywhere.

* Results could be pretty-printed better.

* Generated data could be a bit smarter (in terms of time/location
  making sense; possibly generate "sensible" data then shuffle it a bit?).
